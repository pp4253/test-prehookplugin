package com.att.hook;

import com.atlassian.stash.user.StashAuthenticationContext;
import com.atlassian.stash.user.StashUser;

import java.util.Collection;
import com.atlassian.stash.hook.HookResponse;
import com.atlassian.stash.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.repository.RefChange;

public class MyPreReceiveRepositoryHook implements PreReceiveRepositoryHook
{
	
	
	
	private final StashAuthenticationContext authenticationContext;

	    public MyPreReceiveRepositoryHook(StashAuthenticationContext authenticationContext) {
	        this.authenticationContext = authenticationContext;
	    }
	
    /**
     * Disables deletion of branches
     */
    @Override
    public boolean onReceive(RepositoryHookContext context, Collection<RefChange> refChanges, HookResponse hookResponse)
    {
    	StashUser currentUser = authenticationContext.getCurrentUser();
    	System.out.println("=========================================");
        return false;
    }
}
